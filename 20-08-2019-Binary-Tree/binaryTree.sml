(*
	1. Define the data type 'a tree that captures a binary tree.
*)

datatype 'a tree = null | node of 'a tree * 'a * 'a tree

(*
	2. Define the in-order, pre-order and post-order traversal of the binary tree returning the list of nodes in the given order. First write down the type of the function(s) and then go about defining them.
*)

(*
	inorder : 'a tree -> 'a list
*)
fun inorder null = [] 
	| inorder (node(x, a, y)) = inorder x @ a :: inorder y;

(*
	preorder : 'a tree -> 'a list
*)
fun preorder null = [] 
	| preorder (node(x, a, y)) = a :: preorder x @ preorder y

(*
	postorder : 'a tree -> 'a list
*)
fun postorder null = [] 
	| postorder (node(x, a, y)) = postorder x @ postorder y @ [a]

(*
	tree1 : 
			1
	   	   / \
	  	  2   3
*)

val tree1 = (node(node(null, 2, null), 1, node(null, 3, null)))
val ino = inorder tree1
val pre = preorder tree1
val post = postorder tree1

(*
	3. Define the map function for the tree data type.
	map : ('a -> 'b) -> 'a tree = 'b tree
	map f : 'a tree -> 'b tree
	map f 'a tree : 'b tree
*)

fun map f (node(x, a, y)) = node((map f x), f a, (map f y))
	| map f null = null

(*
	tree2 : 
			1
	   	   / \
	  	  2   3
*)
fun inc x = x + 1
val tree2 = (node(node(null, 2, null), 1, node(null, 3, null)))
val mapped = map inc tree2

(*
	4. Define the recursor function for the tree data type. First write down the type of the recursor and then write its definition.
*)

(*
	recursor : b -> (b * 'a * b) -> 'a tree -> b
	recursor b : (b * 'a * b) -> 'a tree -> b
*)

fun recursor b f null = b
	| recursor b f (node(x, a, y)) = f(recursor b f x, a, recursor b f y)

(*
	Preorder, Inorder and Postorder using recursion principle
*)

fun fin(x, a, y) = x @ [a] @ y
fun fpre(x, a, y) = a :: x @ y
fun fpost(x, a, y) = x @ y @ [a]

val recurPre = recursor [] fpre tree2
val recurIn = recursor [] fin tree2
val recurPos = recursor [] fpost tree2

(*
	5. Define the rotate clockwise function for binary trees. The rotate clockwise the function is pictorially defined as the following.	
*)

(*
	rotate : 'a tree -> 'a tree
*)

fun rotate (node(node(t1, b, t2), a, t3)) = node(t1, b, node(t2, a, t3))
	| rotate x = x

(*
	tree3 : 					rot :
			1							2
	   	   / \                         / \
	  	  2   3                       4   1
		 / \                             / \
		4   5                           5   3
*)
val tree3 = (node(node(node(null, 4, null), 2, node(null, 5, null)), 1, node(null, 3, null)))
val rot = rotate tree3
val inord = inorder rot
(*
fun sum (x, a, y) = x + a + y
val sumtree = recursor 0 sum tree3
*)
