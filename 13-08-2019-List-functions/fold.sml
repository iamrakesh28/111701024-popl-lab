(*
	range : int -> int -> int list
	range n : -> int -> int list
	range n m : int list
*)

fun range n m = if n < m then n :: range (n + 1) m 
	else n :: []

(*
	Factorial using foldr function from library and range function
*)

fun factorialr n = foldr op * 1 (range 1 n)
val factr = factorialr 5

(*
	Factorial using foldl function from library and range function
*)

fun factoriall n = foldl op * 1 (range 1 n)

val factl = factoriall 5
