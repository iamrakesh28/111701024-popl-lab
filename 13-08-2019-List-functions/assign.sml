(*
 1. Write the function isEmpty : 'a list -> bool which checks whether a list is empty or not.
*)

fun isEmpty [] = true 
	| isEmpty _ = false

val list1 = []
val list2 = [1, 2, 3, 4]

(*
 2. The map function is defined in English as follows: map takes a function f and a list l and applies to all arguments of the list. 
*)

(*
	map : ('a -> 'b) -> 'a list -> 'b list
	map f : 'a list -> 'b list
	map f 'a list -> 'b list	
*)

fun map f (x :: xs) = f x :: (map f xs) 
	| map f [] = []

fun inc x = x + 1

val list3 = map inc list1
val list4 = map inc list2

(*
	f : 'a -> 'b -> 'b
	val foldr : ('a -> 'b -> 'b) -> 'a list -> 'b -> 'b
	foldr f : 'a list -> 'b -> 'b
	foldr f 'a list : 'b -> 'b
	foldr f 'a list 'b -> 'b
*)

fun plus x y = x + y
fun sub x y = x - y
val b = 5

fun foldr f (x :: xs) b = f x (foldr f xs b)
	| foldr f [] b = b

val sumr = foldr plus list2 b
val subtr = foldr sub list2 b

(*
	Testing..
	list2 = [1, 2, 3, 4]
	b = 5
	sumr = (1 + (2 + (3 + (4 + 5)))) = 15
	subtr = (1 - (2 - (3 - (4 - 5)))) = 3
*)

(*
	f : 'b -> 'a -> 'b
	val foldl : ('b -> 'a -> 'b) -> 'b -> 'a list -> 'b
	foldl f : 'b -> 'a list -> 'b
	foldl f 'b : 'a list -> 'b
	foldr f 'b 'a list-> 'b
*)

fun foldl f b (x :: xs) = foldl f (f b x) xs
	| foldl f b [] = b

(*
	Testing..
	list2 = [1, 2, 3, 4]
	b = 5
	suml = ((((5 + 1) + 2) + 3) + 4) = 15
	subtl = ((((5 - 1) - 2) - 3) - 4) = -5
*)

val suml = foldl plus b list2
val subtl = foldl sub b list2

(*
3. Write the function range n m which gives the list [n,n+1,...,m]
*)

(*
	range : int -> int -> int list
	range n : -> int -> int list
	range n m : int list
*)

fun range n m = if n < m then n :: range (n + 1) m 
	else n :: []

(*
	Factorial using foldr and range function
*)
fun mul x y = x * y
fun factorialr n = foldr mul (range 1 n) 1
val factr = factorialr 5
(*
	Factorial using foldl and range function
*)

fun factoriall n = foldl mul 1 (range 1 n)

val factl = factoriall 5
