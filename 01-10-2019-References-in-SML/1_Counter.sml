signature COUNTER = sig
    val incr : unit -> unit
    val set : int -> unit
    val get : unit -> int		     
end		     

structure Counter : COUNTER = struct
    val x = ref 0	
    fun incr () = let
	             val _ = x := !x + 1
                  in
   	             ()
                  end
    fun set v = let
	           val _ = x := v
                in
		    ()
                end
    val get = fn () => !x
end			   
			  
val empty1 = Counter.set 3
val empty2 = Counter.incr ()
val value = Counter.get ()		   
		  
		  
