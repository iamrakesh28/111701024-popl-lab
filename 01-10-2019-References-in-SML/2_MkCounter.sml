signature COUNTER = sig
    val incr : unit -> unit
    val set : int -> unit
    val get : unit -> int
end			  

functor MkCounter () : COUNTER = struct
    val x = ref 0	
    fun incr () = let
	             val _ = x := !x + 1
                  in
   	             ()
                  end
    fun set v = let
	           val _ = x := v
                in
		    ()
                end
    val get = fn () => !x
end

structure A = MkCounter ()
structure B = MkCounter ()

val a = A.set 42
val b = B.set 56
	      
val c = A.get (A.incr ())
val d = B.set (A.get ())	      
val x = B.get ()
