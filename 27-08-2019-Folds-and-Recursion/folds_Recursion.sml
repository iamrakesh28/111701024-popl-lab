(*
	Instead of using explicit recursion, define the following library function in terms of either foldr or foldl which ever is convenient. For the documentation of these library function, read the documentation of the List structure

*)

datatype 'a option = none | some of 'a
val t1 = [1, 2, 3, 4, 5, 6]

(*
	1. partition : ('a -> bool) -> 'a list -> 'a list * 'a list
	val foldl : ('a * 'a list * 'a list -> 'a list * 'a list) -> 'a list * 'a list -> 'a list -> 'a list * 'a list 
*)

fun eval f (x, (pos, neg)) = if f x = true then (pos @ [x], neg)
							else (pos, neg @ [x])

fun partition f x = foldl (eval f) ([], []) x

fun check1 x = if x > 3 then true
				else false

val v1 = partition check1 t1

(*
	2. map : ('a -> 'b) -> 'a list -> 'b list
	val foldr : ('a * 'b list -> 'b list) -> 'b list -> 'a list -> 'b list
*)

fun F f (x, y) = f x :: y
fun map f x = foldr (F f) [] x

fun fmap x = x + 1
val v2 = map fmap t1


(*
	3. reverse : 'a list -> 'a list
	val foldl : ('a * 'b list -> 'b list) -> 'b list -> 'a list -> 'b list
*)

fun R (x, y) = x :: y
fun reverse x = foldl R [] x

val v3 = reverse t1 


(*
	4. nth : 'a list * int -> 'a option
	val foldl : ('a * (int -> 'a option) -> (int -> 'a option)) -> (int -> 'a option) -> 'a list-> (int -> 'a option)
*)

fun base _ = none
fun help (x, f) = fn n => if n = 0 then some(x) else f (n - 1)
fun nth (x, n) = (foldr help base x) n

(*
	nth using explicit recursion
	fun nth ([], _) = none
		| nth (x :: _, 0) = some(x)
		| nth (_ :: xs, i) = nth(xs, i - 1)
*)

val v4 = nth(t1, ~1)
val v5 = nth(t1, 0)
val v6 = nth(t1, 5)
