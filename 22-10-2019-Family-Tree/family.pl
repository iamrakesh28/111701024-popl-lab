man(arjun).
man(pandu).
man(bheem).
man(yudhistir).
man(sahadev).
man(nakul).
man(abhi).    
man(vyas).
woman(kunti).
woman(madri).
woman(sis).    
parent(pandu, vyas).
parent(arjun, pandu).   
parent(arjun, kunti).
parent(bheem, pandu).
parent(bheem, kunti).
parent(yudhistir, pandu).
parent(yudhistir, kunti).
parent(abhi, arjun).    

parent(nakul, pandu).
parent(nakul, madri).
parent(sahadev, pandu).
parent(sahadev, madri).      

%%% Facts end here %%%%%%%%%%%%%%%%%%%

mother(X,Y)  :- woman(Y), parent(X,Y).
father(X,Y)  :- man(Y), parent(X,Y).
sibling(X,Y) :- dif(X,Y), father(X,Z), father(Y,Z), mother(X,M), mother(Y,M).
brother(X,Y) :- dif(X,Y), father(X,Z), father(Y,Z), mother(X,M), mother(Y,M), man(Y).
sister(X,Y)  :- dif(X,Y), father(X,Z), father(Y,Z), mother(X,M), mother(Y,M), woman(Y).
ancestor(X,Y):- parent(X,Y).
ancestor(X,Y):- parent(X,Z), ancestor(Z,Y).
halfbrother(X,Y) :- dif(X,Y), parent(X,Z), parent(Y,Z), not(brother(X,Y)), man(Y).

%% Exercise
%%
%% What happens when the brother definition does not have dif
%% if dif is present then brother(arjun, arjun) will return true
%% Write a half brother relation similar to brother.
