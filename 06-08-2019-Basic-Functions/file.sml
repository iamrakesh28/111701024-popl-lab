(*
1. Tri-variate version of the curry and uncurry functions
 *)
(*
	curry function

	fun curry : ('a * 'b * 'c -> 'd) -> 'a -> 'b -> 'c -> 'd
	fun curry f : 'a -> 'b -> 'c -> 'd
	fun curry f x : 'b -> 'c -> 'd
	fun curry f x y : 'c -> 'd
	fun curry f x y z : 'd
*)
fun curry f x y z = f (x, y, z)
val curry_ = fn f => fn x => fn y => fn z => f(x, y, z)
(*
	uncurry function

	fun uncurry : ('a -> 'b -> 'c -> 'd) -> 'a * 'b * 'c -> 'd
	fun uncurry f : 'a * 'b * 'c -> 'd
	fun uncurry f (x, y, z) : 'd
*)
fun uncurry f (x, y, z) = f x y z
val uncurry_ = fn f => fn (x, y, z) => f x y z

(*
2. fst : 'a * 'b * 'c -> 'a , snd : 'a * 'b * 'c -> 'b, and thd : 'a * 'b * 'c -> 'c.
 *)

(*
	fun fst : 'a * 'b * 'c -> 'a
	fun fst (x, y, z) : 'a
	fun fst (x, y, z) = x
*)
fun fst (x, _, _) = x

(*
	fun snd : 'a * 'b * 'c -> 'b
	fun fst (x, y, z) : 'b
	fun fst (x, y, z) = y
*)
fun fst (_, y, _) = y

(*
	fun thd : 'a * 'b * 'c -> 'c
	fun fst (x, y, z) : 'c
	fun fst (x, y, z) = z
*)
fun fst (_, _, z) = z

(*
3. The plus, mul, and pow functions for the Nat type defined in the class. Also write the helper function toInt : Nat -> int which will aid in debugging the above function
*)

datatype natural = zero | succ of natural

(* 
	fun plus x y = x + y
*)
fun plus zero y = y | plus (succ(x)) y = succ(plus x y)

(* 
	fun mul x y = x * y
*)

fun mul zero y = zero | mul (succ(x)) y = plus y (mul x y)
(*
	fun pow x y = x ^ y
	pow (succ(succ(zero))) (succ(succ(succ(zero))))
*)
fun pow x zero = succ(zero) | pow x (succ(y)) = mul x (pow x y)
(* 
	toInt : Nat -> int
*)
fun toInt zero = 0 | toInt (succ(x)) = 1 + toInt(x);

(* 
4. The iterate function that takes as argument an n : Nat, a function f, and a starting value x0 and computes f (f ... (f x0)) (f applied on x0, n-times). Rework plus, mul, and pow using iterate.
*)

(*
	iterate : natural -> ('a -> 'a) -> 'a -> 'a
*)
fun iterate zero f x0 = x0 | iterate (succ(n)) f x0 = iterate n f (f x0)

(* 
	fun plus x y = x + y
*)
fun plus x y = iterate x succ y

(* 
	fun mul x y = x * y
*)

fun mul zero y = zero | mul (succ(x)) y = iterate x (plus y) y
(*
	fun pow x y = x ^ y
	pow (succ(succ(zero))) (succ(succ(succ(zero))))
*)
fun pow x zero = succ(zero) | pow x (succ(y)) = iterate y (mul x) x
