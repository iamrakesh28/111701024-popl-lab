signature SORT = sig
	  type t
	  val sort : t list -> t list
end


functor QSort (O : ORD_KEY) : SORT = struct
	type t = O.ord_key
	fun partitionL p nil = nil
	    | partitionL p (x :: xs) = if O.compare(x, p) = LESS then x :: partitionL p xs
	      		     	     else partitionL p xs
	fun partitionG p nil = nil
	    | partitionG p (x :: xs) = if O.compare(x, p) = GREATER orelse O.compare(x, p) = EQUAL
	      		     	     then x :: partitionG p xs else partitionG p xs		     
	fun sort nil = nil
	    | sort (x :: xs) = sort (partitionL x xs) @ [x] @ sort (partitionG  x xs)
end				     

structure IntOrd : ORD_KEY = struct
	  type ord_key = int
	  val compare = fn (a : ord_key, b : ord_key) => Int.compare(a, b)
end	  

structure sort_int = QSort (IntOrd)

val listA = [3, 4, 6, 8, 1, 13]
val sort_listA = sort_int.sort listA

val listB = [9, 21, 3, 98, 11]
val sort_listB = sort_int.sort listB
