(* datatype 'a option = SOME of 'a | NONE *)

datatype monotype = var of Atom.atom
		  | basic of Atom.atom
		  | arrow of monotype * monotype

fun look x (arrow(a, b)) = look x a orelse look x b 
  | look x (var a) = if Atom.compare(x, a) = EQUAL then true else false 
  | look x _  = false
		    
(*
val test = (arrow(var("ab"), var("bc")))
val f1 = look "ac" test
val f2 = look "ab" test
*)

fun subst tele (var x) = ( case AtomMap.find (tele, x) of
			       (SOME tau) => subst tele tau
     			     | (NONE)  => var x
			 )   

  | subst tele (basic a) = basic a
  | subst tele (arrow (a, b)) = arrow(subst tele a, subst tele b)

fun substpair tele (x, y) = (subst tele x, subst tele y)

fun opt (SOME x) = x
    | opt (NONE) = AtomMap.empty

fun unifyList ((x1, x2) :: xs) = let
				    val tele = opt (unify x1 x2)
                		    val xss = List.map (substpair tele) xs
    				    val telexs = unifyList xss
                                 in
				     AtomMap.unionWith (fn (a, b) => a) (tele, telexs)
                                 end
  | unifyList [] = AtomMap.empty				 				 

and unify (var x) (arrow(a, b)) = if look x a orelse look x b = true
				   then NONE
				  else SOME (AtomMap.singleton (x, arrow(a, b)))
  | unify (arrow(a, b)) (var x) = if look x a orelse look x b = true
				   then NONE
				   else SOME (AtomMap.singleton (x, arrow(a, b)))
  | unify (arrow(a, b)) (arrow(c, d)) = SOME (unifyList [(a, c), (b, d)])  
  | unify (var x) (var y) = SOME (AtomMap.singleton (x, var y))
  | unify (var x) (basic a) = SOME (AtomMap.singleton (x, basic a))
  | unify (basic a) (var x) = SOME (AtomMap.singleton (x, basic a))
  | unify (basic a) (basic b) = if Atom.compare(a, b) = EQUAL
				then SOME AtomMap.empty
				else NONE
  | unify _ _  = NONE


(*
	Testing code
*)

datatype stringMono = V of string
	 	     | B of string
		     | A of stringMono * stringMono

fun convertToAtom (V x) = var (Atom.atom x)
    | convertToAtom (B x) = basic (Atom.atom x)
    | convertToAtom (A (x, y)) = arrow (convertToAtom x, convertToAtom y)
    
fun convertToString (var x) = V (Atom.toString x)
    | convertToString (basic x) = B (Atom.toString x)
    | convertToString (arrow (x, y)) = A (convertToString x, convertToString y)


fun convertListA ((x1, x2) :: xs) = (convertToAtom x1, convertToAtom x2) :: convertListA xs
    | convertListA [] = []
    
fun convertList ((x1, x2) :: xs) = (Atom.toString x1, convertToString x2) :: convertList xs
    | convertList [] = []
    

val test1 = [(V "b", B "true"), (V "a", V "b")]
val atest1 = convertListA test1
val lst = AtomMap.listItemsi (unifyList atest1)
val lstS = convertList lst


val test2 = [(A (V "a", A (V "b", V "c")), A (B "bool", A (B "int", B "int")))]
val atest2 = convertListA test2
val lst2 = AtomMap.listItemsi (unifyList atest2)
val lstS2 = convertList lst2

