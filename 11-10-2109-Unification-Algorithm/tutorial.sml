(*
datatype t = cons of bool * t | abs of t -> bool
val a = abs(fn x => true)
*)
datatype mono = var of string
	 | bool
	 | func of mono * mono

val a = var("alpha")

datatype sch = mono
	 | all of string * sch
val b = a
open SMLofNJ.Cont
