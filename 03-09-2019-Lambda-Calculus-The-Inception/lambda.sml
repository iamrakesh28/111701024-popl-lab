(*
	1. Define an ML datatype expr that capture lambda calculus expressions. You may use strings to represent variables.
*)

datatype expr = var of string
			| apply of expr * expr
			| lambda of expr * expr


(*
	e1 = lambda x. y x
*)

val e1 = lambda(var("x"), apply(var("y"), var("x")))

(*
    2. Write functions free : expr -> string list and bound : expr -> string list that computes free and bound variables in a given lambda calculus expression respectively.
	
	free : expr -> string
	bound : expr -> string
	find : string list -> string -> bool
	free_ : string list -> expr -> string list
	free : expr -> string list
*)

fun bound (lambda(var(x), xs)) = x :: bound xs
	| bound (apply(x, y)) = bound x @ bound y
	| bound (var(_)) = []

(*
	e2 = lambda x. (lambda y. y) x
*)

val e2 = lambda(var("x"), apply(lambda(var("y"), var("y")), var("x")))
val b1 = bound e1
val b2 = bound e2

fun find nil x = false
	| find (x :: xs) y = if (x = y) then true else find xs y

val f1 = ["x", "xy"]
val found1 = find f1 "xy"
val found2 = find f1 "z"

fun free_ ls (lambda(var(x), xs)) = free_ (x :: ls) xs
	| free_ ls (apply(x, y)) = free_ ls x @ free_ ls y
	| free_ ls (var(x)) = if (find ls x = true) then [] else [x]

val free = free_ []

val free1 = free e1
val free2 = free e2

(*
	e3 = (lambda x. y x) (z t lambda y. y)
*)

val e3 = apply(lambda(var("x"), apply(var("y"), var("x"))), apply(apply(var("z"), var("t")), lambda(var("y"), var("y"))))

val free3 = free e3
val b3 = bound e3

(*
    3 . Define a function fresh : string list -> string that computes a fresh variable name, i.e. fresh xs gives a string x which is different from all strings in xs. (Hint use diagonalisation).
	
	fresh : string list -> string
	size : string -> int
	max_string : string list -> string
	
*)

fun max_string nil = ""
	| max_string (x::xs) = 
		let val a = max_string xs
		in
			if size(x) > size(a) then x else a
		end 
fun fresh x = concat ((max_string x) :: ["a"])
val f2 = ["a", "b", "xy"]
val fresh1 = fresh f1

(*
    4. Write a function subst : expr -> string -> expr, where subst e₁ x e₂ substitutes in e₁ all free occurance of x by e₂.

	subst : expr -> string -> expr -> expr
	e1 = lambda x. y x
	e2 = lambda x. (lambda y. y) x
	
*)

fun subst (var(x)) y e2 = if x = y then e2 else var(x)
	| subst (apply(a, b)) x e2 = apply(subst a x e2, subst b x e2)
	| subst (lambda(var(a), y)) x e2 = if x = a then lambda(var(a), y) else lambda(var(a), subst y x e2)

val subst1 = subst e1 "y" e1
